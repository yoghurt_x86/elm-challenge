module Main exposing (..)

import Browser
import Html as Html
import Html.Attributes
import Html.Events
import Http
import Json.Encode as E
import Json.Decode as D

-- MODEL
type State 
  = Default
  | Failure
  | Loading
  | Success String
type alias Form = {username : String, password : String}
type alias Model = {form: Form, state: State}


init : () -> (Model, Cmd Msg)
init _ =
  ( {state = Default, form = {username = "", password = ""}}
  , Cmd.none
  )

-- UPDATE
type Msg
  = Input
  | Username String
  | Password String
  | Login (Result Http.Error String)
  | FetchData (Result Http.Error String)


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Username s -> ({model | form = {username = s, password = model.form.password}}, Cmd.none) 
    Password s -> ({model | form = {username = model.form.username, password = s}}, Cmd.none) 
    Input ->   ({model | state = Loading}, login model.form.username model.form.password)
    Login r -> 
      case Debug.log "r" r of 
       Ok s -> ({model | state = Loading}, fetchName s)
       Err _ -> ({model | state = Failure}, Cmd.none)
    FetchData r ->
      case Debug.log "r" r of 
       Ok s -> ({model | state = Success s}, Cmd.none)
       Err _ -> ({model | state = Failure}, Cmd.none)
      


-- VIEW
span : String -> Html.Html Msg
span s = 
  Html.span [] [Html.text s]

loginStatus : Model -> Html.Html Msg
loginStatus model = 
  case model.state of
    Default -> span "Please Login"
    Loading -> span "Loading..."
    Failure -> span "Failed... Try again"
    Success s -> span s    

view : Model -> Html.Html Msg
view model =
  Html.div [] [
    Html.input [
      Html.Attributes.type_ "text", 
      Html.Attributes.placeholder "Username",
      Html.Attributes.value model.form.username,
      Html.Events.onInput Username
     ] [],
    Html.input [
      Html.Attributes.type_ "password", 
      Html.Attributes.placeholder "password",
      Html.Attributes.value model.form.password,
      Html.Events.onInput Password
     ] [],
    Html.button [Html.Events.onClick Input] [Html.text "login"],
    loginStatus model 
  ]

loginquery : String -> String -> String
loginquery email pass = """mutation login {
  mutatePerson {
    login(email: \"""" ++ email ++ """\", password: \"""" ++ pass ++ """\") {
      ... on Token {
        token
      }
      ... on MutationError {
        errors {
          message
        }
      }
    }
  }
}"""
namequery = """query get {
  me {
    name
  }
}"""
toJson : String -> E.Value
toJson s = 
  E.object [("query", E.string s)]

login : String -> String -> Cmd Msg
login email password =
  Http.post
  { url = "https://lendinotest.herokuapp.com/graphql",
    body = Http.jsonBody <| toJson <| loginquery email password, 
    expect = Http.expectJson Login loginDecoder
  }

loginDecoder = 
  D.field "data" <| D.field "mutatePerson" <| D.field "login" <| D.field "token" D.string


fetchName : String -> Cmd Msg
fetchName token = 
  Http.request
  { method = "POST",
    url = "https://lendinotest.herokuapp.com/graphql",
    headers = [Http.header "Content-type" "application/json",
               Http.header "Authorization" token], 
    body = Http.jsonBody <| toJson <| namequery,
    expect = Http.expectJson FetchData nameDecoder,
    timeout = Nothing,
    tracker = Nothing
  }

nameDecoder = 
  D.field "data" <| D.field "me" <| D.field "name" D.string




subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.none

-- MAIN

main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }